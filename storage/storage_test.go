package storage

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/vitalyo/automobile/data"
)

func TestStorage(t *testing.T) {
	s := NewInMem()
	assert.NotNil(t, s)

	defer s.Close()

	c := &data.Car{
		Brand: "Toyota",
		Model: "Mark II",
		Price: 20000,
	}

	id, err := s.Create(c)
	assert.NoError(t, err)
	assert.Equal(t, id, uint(1))

	_, err = s.Read(1000)
	assert.EqualError(t, err, ErrorNotFound.Error())

	cRead, err := s.Read(id)
	assert.NoError(t, err)
	assert.True(t, assert.ObjectsAreEqual(c, cRead))

	c.Status = data.CarStatusInStock
	err = s.Update(1000, c)
	assert.EqualError(t, err, ErrorNotFound.Error())

	err = s.Update(id, c)
	assert.NoError(t, err)

	cRead, err = s.Read(id)
	assert.NoError(t, err)
	assert.True(t, assert.ObjectsAreEqual(c, cRead))

	err = s.Delete(1000)
	assert.EqualError(t, err, ErrorNotFound.Error())

	err = s.Delete(id)
	assert.NoError(t, err)

	_, err = s.Read(id)
	assert.EqualError(t, err, ErrorNotFound.Error())
}
