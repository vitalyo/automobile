package storage

import (
	"gitlab.com/vitalyo/automobile/data"
)

const (
	cmdCreate uint = iota
	cmdRead
	cmdUpdate
	cmdDelete
	cmdClose
)

type responseInMem struct {
	error error
	id    uint
	car   *data.Car
}

type requestInMem struct {
	cmd      uint
	chanResp chan *responseInMem
	id       uint
	car      *data.Car
}

// It is in memory storage
type inMem struct {
	storage map[uint]*data.Car
	lastId  uint
	chanReq chan *requestInMem
}

// Create in memory storage
func NewInMem() Storager {
	s := &inMem{
		storage: make(map[uint]*data.Car),
		chanReq: make(chan *requestInMem),
	}
	go s.run()
	return s
}

func (s *inMem) run() {
	defer close(s.chanReq)

	for req := range s.chanReq {
		resp := new(responseInMem)

		switch req.cmd {

		case cmdCreate:
			s.lastId++
			resp.id = s.lastId
			s.storage[resp.id] = req.car

			req.chanResp <- resp

		case cmdRead:
			var ok bool
			if resp.car, ok = s.storage[req.id]; ok {
				resp.id = req.id
			} else {
				resp.error = ErrorNotFound
			}
			req.chanResp <- resp

		case cmdUpdate:
			if _, ok := s.storage[req.id]; ok {
				s.storage[req.id] = req.car
			} else {
				resp.error = ErrorNotFound
			}
			req.chanResp <- resp

		case cmdDelete:
			resp := new(responseInMem)
			if _, ok := s.storage[req.id]; ok {
				delete(s.storage, req.id)
			} else {
				resp.error = ErrorNotFound
			}
			req.chanResp <- resp

		case cmdClose:
			break
		}
	}

	return
}

// Create make car in storage
func (s *inMem) Create(aCar *data.Car) (uint, error) {
	chResp := make(chan *responseInMem)
	req := &requestInMem{
		cmd:      cmdCreate,
		chanResp: chResp,
		car:      aCar,
	}

	s.chanReq <- req

	resp := <-chResp
	return resp.id, resp.error
}

// Read reads car from storage
func (s *inMem) Read(id uint) (*data.Car, error) {
	chResp := make(chan *responseInMem)
	req := &requestInMem{
		cmd:      cmdRead,
		chanResp: chResp,
		id:       id,
	}

	s.chanReq <- req

	resp := <-chResp
	return resp.car, resp.error
}

// Update changes car
func (s *inMem) Update(id uint, aCar *data.Car) error {
	chResp := make(chan *responseInMem)
	req := &requestInMem{
		cmd:      cmdUpdate,
		chanResp: chResp,
		car:      aCar,
		id:       id,
	}

	s.chanReq <- req

	resp := <-chResp
	return resp.error
}

// Delete deletes car
func (s *inMem) Delete(id uint) (err error) {
	chResp := make(chan *responseInMem)
	req := &requestInMem{
		cmd:      cmdDelete,
		chanResp: chResp,
		id:       id,
	}

	s.chanReq <- req

	resp := <-chResp
	return resp.error
}

// Close closes storage
func (s *inMem) Close() {
	req := &requestInMem{
		cmd: cmdClose,
	}

	s.chanReq <- req
}
