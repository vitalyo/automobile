package storage

import (
	"errors"

	"gitlab.com/vitalyo/automobile/data"
)

// Storager is interface db
type Storager interface {
	Create(car *data.Car) (uint, error)
	Read(id uint) (*data.Car, error)
	Update(id uint, car *data.Car) error
	Delete(id uint) error
	Close()
}

var (
	ErrorNotFound = errors.New("not found")
)
