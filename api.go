package automobile

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"gitlab.com/vitalyo/automobile/data"
	"gitlab.com/vitalyo/automobile/storage"
)

func NewApi(db storage.Storager) *chi.Mux {
	mux := chi.NewMux()

	mux.Method(http.MethodPost, "/create", NewCreate(db))
	mux.Method(http.MethodGet, "/read/{id:\\d+}", NewRead(db))
	mux.Method(http.MethodPut, "/update/{id:\\d+}", NewUpdate(db))
	mux.Method(http.MethodDelete, "/delete/{id:\\d+}", NewDelete(db))

	return mux
}

type hBase struct {
	db storage.Storager
}

func (h hBase) getData(w http.ResponseWriter, r *http.Request) (car map[string]string, err error) {
	defer r.Body.Close()
	dec := json.NewDecoder(r.Body)
	car = make(map[string]string)
	if err = dec.Decode(&car); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	return
}

func (h hBase) result(w http.ResponseWriter, result []byte) {
	w.Header().Set("Content-Type", "application/json")
	w.Write(result)
}

type hCreate struct {
	hBase
}

func NewCreate(db storage.Storager) hCreate {
	return hCreate{
		hBase: hBase{db: db},
	}
}

func (h hCreate) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	d, err := h.getData(w, r)
	if err != nil {
		return
	}

	dbCar, err := data.NewCar(d)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	id, err := h.db.Create(dbCar)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	carID := map[string]string{
		"id": fmt.Sprintf("%d", id),
	}
	idByte, err := json.Marshal(carID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	h.result(w, idByte)
}

type hRead struct {
	hBase
}

func NewRead(db storage.Storager) hRead {
	return hRead{
		hBase: hBase{db: db},
	}
}

func (h hRead) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "id")
	id, _ := strconv.Atoi(idStr)

	car, err := h.db.Read(uint(id))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	carByte, err := json.Marshal(car.ToMap())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	h.result(w, carByte)
}

type hUpdate struct {
	hBase
}

func NewUpdate(db storage.Storager) hUpdate {
	return hUpdate{
		hBase: hBase{db: db},
	}
}

func (h hUpdate) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	d, err := h.getData(w, r)
	if err != nil {
		return
	}

	dbCar, err := data.NewCar(d)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	idStr := chi.URLParam(r, "id")
	id, _ := strconv.Atoi(idStr)

	err = h.db.Update(uint(id), dbCar)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

type hDelete struct {
	hBase
}

func NewDelete(db storage.Storager) hDelete {
	return hDelete{
		hBase: hBase{db: db},
	}
}

func (h hDelete) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	idStr := chi.URLParam(r, "id")
	id, _ := strconv.Atoi(idStr)

	err := h.db.Delete(uint(id))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
