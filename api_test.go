package automobile

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/vitalyo/automobile/data"
	"gitlab.com/vitalyo/automobile/storage"
)

func TestApi(t *testing.T) {
	db := storage.NewInMem()
	defer db.Close()

	testData := []struct {
		brand string
		price uint
	}{
		{brand: "Toyota", price: 1000},
		{brand: "BMW", price: 100},
		{brand: "ЗАЗ", price: 10},
	}

	mux := NewApi(db)

	for _, tData := range testData {
		tData := tData
		t.Run(tData.brand, func(t *testing.T) {

			t.Parallel()

			ts := httptest.NewServer(mux)

			resp, err := http.Get(ts.URL)
			assert.NoError(t, err)
			assert.Equal(t, resp.StatusCode, http.StatusNotFound)

			resp, _ = http.Get(ts.URL + "/create")
			assert.Equal(t, resp.StatusCode, http.StatusMethodNotAllowed)

			resp, err = http.Post(ts.URL+"/create", "application/json", strings.NewReader(``))
			assert.Equal(t, resp.StatusCode, http.StatusBadRequest)

			resp, err = http.Post(ts.URL+"/create", "application/json", strings.NewReader(`{"id": "125"}`))
			assert.Equal(t, resp.StatusCode, http.StatusBadRequest)
			body, _ := ioutil.ReadAll(resp.Body)
			resp.Body.Close()
			assert.Contains(t, string(body), data.ErrorNotBrand.Error())

			reqJSON := `{
"brand": "%s",
"model": "Mark II",
"price": "%d"
}`

			req, err := http.NewRequest(http.MethodPost, ts.URL+"/create",
				strings.NewReader(fmt.Sprintf(reqJSON, tData.brand, 123456)))
			assert.NoError(t, err)
			req.Header.Set("Content-Type", "application/json")
			resp, _ = ts.Client().Do(req)

			m := make(map[string]string)
			dec := json.NewDecoder(resp.Body)
			err = dec.Decode(&m)
			assert.NoError(t, err)

			id, ok := m["id"]
			if !ok {
				t.Error("haven't key 'id'")
			}
			resp.Body.Close()

			resp, _ = http.Get(ts.URL + "/read")
			assert.Equal(t, resp.StatusCode, http.StatusNotFound)

			resp, _ = http.Get(ts.URL + "/read/id")
			assert.Equal(t, resp.StatusCode, http.StatusNotFound)

			resp, _ = http.Get(ts.URL + "/read/01id")
			assert.Equal(t, resp.StatusCode, http.StatusNotFound)

			resp, _ = http.Get(ts.URL + "/read/id01")
			assert.Equal(t, resp.StatusCode, http.StatusNotFound)

			resp, _ = http.Get(ts.URL + "/read/" + id)
			assert.Equal(t, resp.StatusCode, http.StatusOK)

			dec = json.NewDecoder(resp.Body)
			err = dec.Decode(&m)
			assert.NoError(t, err)
			resp.Body.Close()
			assert.Equal(t, m["brand"], tData.brand)

			req, err = http.NewRequest(http.MethodPut, ts.URL+"/update/"+id,
				strings.NewReader(fmt.Sprintf(reqJSON, tData.brand, tData.price)))
			assert.NoError(t, err)
			req.Header.Set("Content-Type", "application/json")
			resp, _ = ts.Client().Do(req)
			assert.Equal(t, resp.StatusCode, http.StatusOK)

			resp, _ = http.Get(ts.URL + "/read/" + id)
			assert.Equal(t, resp.StatusCode, http.StatusOK)

			dec = json.NewDecoder(resp.Body)
			err = dec.Decode(&m)
			assert.NoError(t, err)
			resp.Body.Close()
			assert.Equal(t, m["brand"], tData.brand)
			assert.Equal(t, m["price"], fmt.Sprintf("%d", tData.price))

			req, err = http.NewRequest(http.MethodDelete, ts.URL+"/delete/"+id, nil)
			assert.NoError(t, err)
			resp, _ = ts.Client().Do(req)
			assert.Equal(t, resp.StatusCode, http.StatusOK)

			resp, _ = http.Get(ts.URL + "/read/" + id)
			assert.Equal(t, resp.StatusCode, http.StatusInternalServerError)

			body, err = ioutil.ReadAll(resp.Body)
			assert.NoError(t, err)
			resp.Body.Close()
			assert.Contains(t, string(body), storage.ErrorNotFound.Error())

			ts.Close()
		})
	}
}
