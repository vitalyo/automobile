package data

import (
	"errors"
	"fmt"
	"strconv"
)

type CarStatus uint

var (
	ErrorBadNumber = errors.New("bad number")
	ErrorNotBrand  = errors.New("haven't brand")
	ErrorNotModel  = errors.New("haven't model")
)

const (
	CarStatusUnknow CarStatus = iota
	CarStatusEnRoute
	CarStatusInStock
	CarStatusSold
	CarStatusTakenOff
)

const (
	carStatusUnknow   = "Не определен"
	carStatusEnRoute  = "В пути"
	carStatusInStock  = "На складе"
	carStatusSold     = "Продан"
	carStatusTakenOff = "Снят с продажи"
)

func NewCarStatus(s string) CarStatus {
	switch s {
	case carStatusEnRoute:
		return CarStatusEnRoute
	case carStatusInStock:
		return CarStatusInStock
	case carStatusSold:
		return CarStatusSold
	case carStatusTakenOff:
		return CarStatusTakenOff
	}
	return CarStatusUnknow
}

func (s CarStatus) String() string {
	switch s {
	case CarStatusEnRoute:
		return carStatusEnRoute
	case CarStatusInStock:
		return carStatusInStock
	case CarStatusSold:
		return carStatusSold
	case CarStatusTakenOff:
		return carStatusTakenOff
	}
	return carStatusUnknow
}

// Car is data of car's model
type Car struct {
	Brand       string
	Model       string
	Price       uint
	Status      CarStatus
	Kilometrage int
}

func NewCar(m map[string]string) (*Car, error) {
	c := new(Car)

	if v, ok := m["brand"]; ok {
		c.Brand = v
	} else {
		return nil, ErrorNotBrand
	}

	if v, ok := m["model"]; ok {
		c.Model = v
	} else {
		return nil, ErrorNotModel
	}

	if v, ok := m["price"]; ok {
		if i, err := strconv.Atoi(v); err != nil {
			return nil, err
		} else if i <= 0 {
			return nil, ErrorBadNumber
		} else {
			c.Price = uint(i)
		}
	}

	if v, ok := m["status"]; ok {
		c.Status = NewCarStatus(v)
	}

	if v, ok := m["kilometrage"]; ok {
		if i, err := strconv.Atoi(v); err != nil {
			return nil, err
		} else {
			c.Kilometrage = i
		}
	}

	return c, nil
}

func (c *Car) ToMap() map[string]string {
	m := make(map[string]string)

	m["brand"] = c.Brand
	m["model"] = c.Model
	m["price"] = fmt.Sprintf("%d", c.Price)
	m["status"] = c.Status.String()
	m["kilometrage"] = fmt.Sprintf("%d", c.Kilometrage)

	return m
}
