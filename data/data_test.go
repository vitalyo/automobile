package data

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCarStataus(t *testing.T) {
	s := CarStatusEnRoute
	assert.Equal(t, s.String(), carStatusEnRoute)
	s = CarStatusInStock
	assert.Equal(t, s.String(), carStatusInStock)
	s = CarStatusSold
	assert.Equal(t, s.String(), carStatusSold)
	s = CarStatusTakenOff
	assert.Equal(t, s.String(), carStatusTakenOff)
	s = CarStatus(100)
	assert.Equal(t, s.String(), carStatusUnknow)

	assert.Equal(t, NewCarStatus(carStatusEnRoute), CarStatusEnRoute)
	assert.Equal(t, NewCarStatus(carStatusInStock), CarStatusInStock)
	assert.Equal(t, NewCarStatus(carStatusSold), CarStatusSold)
	assert.Equal(t, NewCarStatus(carStatusTakenOff), CarStatusTakenOff)
	assert.Equal(t, NewCarStatus(""), CarStatusUnknow)
}

func TestCar(t *testing.T) {
	m := make(map[string]string)
	_, err := NewCar(m)
	assert.EqualError(t, err, ErrorNotBrand.Error())

	m["brand"] = "Toyota"
	_, err = NewCar(m)
	assert.EqualError(t, err, ErrorNotModel.Error())

	m["model"] = "Mark II"
	m["price"] = "a123"
	_, err = NewCar(m)
	assert.Contains(t, err.Error(), "invalid syntax")

	m["price"] = "0"
	_, err = NewCar(m)
	assert.EqualError(t, err, ErrorBadNumber.Error())

	m["price"] = "10000000"
	m["kilometrage"] = "qqq"
	_, err = NewCar(m)
	assert.Contains(t, err.Error(), "invalid syntax")

	m["kilometrage"] = "0"
	_, err = NewCar(m)
	assert.NoError(t, err)
}
